﻿using Factory_Hospital.Builder;
using Factory_Hospital.Models.Patient;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Factory_Hospital
{
    class Program
    {
        static void Main(string[] args)
        {
            var localServiceProvider = InitializeContainer();
            var exporterFactory = new PatientExporterFactory(localServiceProvider);

            var doctorFactory = new DoctorFactory();

            MedicinesFactory medicinesFactory = new AntiviralFactory();
            Patient patient1 = new PatientBuilder()
                .General
                    .Name("Sergey")
                    .PatientType(PatientType.Covid)
                    .Doctor(doctorFactory.Get(PatientType.Covid))
                .HasTreatment
                    .Medicines(medicinesFactory.GetSerious().Take(1));

            medicinesFactory = new AntiColdFactory();
            Patient patient2 = new PatientBuilder()
                .General
                    .Name("Irina")
                    .PatientType(PatientType.Child)
                    .Doctor(doctorFactory.Get(PatientType.Child))
                .HasTreatment
                    .Medicines(medicinesFactory.GetLight().Take(2));

            medicinesFactory = new AntiColdFactory();
            Patient patient3 = new PatientBuilder()
                 .General
                     .Name("Timofey")
                     .PatientType(PatientType.Adult)
                     .Doctor(doctorFactory.Get(PatientType.Adult))
                 .HasTreatment
                     .Medicines(medicinesFactory.GetSerious().Take(1));

            var exporter = exporterFactory.GetExporter(ExporterType.JPEG);
            exporter.Export(patient2);
            exporter.Export(patient1);

            exporter = exporterFactory.GetExporter(ExporterType.DOC);
            exporter.Export(patient3);
        }

        private static ServiceProvider InitializeContainer()
        {
            var serviceCollection = new ServiceCollection();

            serviceCollection.AddScoped<DOCExporterService>()
                             .AddScoped<IPatientExporterService, DOCExporterService>(s => s.GetService<DOCExporterService>());
            serviceCollection.AddScoped<JPEGExporterService>()
                             .AddScoped<IPatientExporterService, JPEGExporterService>(s => s.GetService<JPEGExporterService>());
            return serviceCollection.BuildServiceProvider();
        }
    }
}
