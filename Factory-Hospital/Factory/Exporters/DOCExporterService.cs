﻿using Factory_Hospital.Models.Patient;
using System;
using System.Linq;

namespace Factory_Hospital
{
    public class DOCExporterService : IPatientExporterService
    {
        public void Export(Patient patient)
        {
            Console.WriteLine("----------------------------");
            Console.WriteLine("DOCExporterService::Export");
            Console.WriteLine($"Patient {patient.GeneraInfo.SeqNumber}: {patient.GeneraInfo.Name}");
            Console.WriteLine($"Type: {patient.GeneraInfo.PatientType}");
            Console.WriteLine($"RegistryDate: {patient.GeneraInfo.RegistryDate}");
            Console.WriteLine($"Doctor was set: {patient.GeneraInfo.Doctor.DoctorType}");
            var titles = patient.TreatmentInfo.Treatments
                            .SelectMany(treatment => treatment.Medicines)
                            .Select(medicines => medicines.Title).ToList();
            titles.ForEach(medicineTitle => Console.WriteLine($"Treatment: {medicineTitle}"));
        }
    }
}