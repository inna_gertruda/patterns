﻿using Factory_Hospital.Models.Patient;

namespace Factory_Hospital
{
    public interface IPatientExporterService
    {
        void Export(Patient patient);
    }
}