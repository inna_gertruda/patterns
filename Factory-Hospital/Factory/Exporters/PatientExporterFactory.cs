﻿using Factory_Hospital.Models.Patient;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Factory_Hospital
{
    public class PatientExporterFactory
    {
        private readonly ServiceProvider ServiceProvider;

        public PatientExporterFactory(ServiceProvider localServiceProvider)
        {
            ServiceProvider = localServiceProvider;
        }

        internal IPatientExporterService GetExporter(ExporterType exporterType)
        {
            switch (exporterType)
            {
                case ExporterType.JPEG:
                    return (IPatientExporterService)ServiceProvider.GetService(typeof(JPEGExporterService));
                case ExporterType.DOC:
                    return (IPatientExporterService)ServiceProvider.GetService(typeof(DOCExporterService));
                default:
                    return null;
            }
        }
    }
}