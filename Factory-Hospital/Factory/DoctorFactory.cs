﻿using System;

namespace Factory_Hospital
{
    public class DoctorFactory
    {
        public DoctorFactory()
        {
        }

        internal Doctor Get(PatientType patientType)
        {
            switch (patientType)
            {
                case PatientType.Adult:
                    return Doctor.Factory.CreateTherapist();
                case PatientType.Child:
                    return Doctor.Factory.CreatePediatrician();
                case PatientType.Covid:
                    return Doctor.Factory.CreateInfectionist();
                default:
                    return null;
            }
        }
    }
}