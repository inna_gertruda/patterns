﻿using System.Collections.Generic;

namespace Factory_Hospital
{
    class AntiColdFactory : MedicinesFactory
    {
        public override List<Medicine> GetLight()
        {
            return new List<Medicine>()
            {
                new Medicine("AntiFlu"),
                new Medicine("Nurofen"),
            };
        }

        public override List<Medicine> GetSerious()
        {
            return new List<Medicine>()
            {
                new Medicine("Ameksin"),
            };
        }
    }
}