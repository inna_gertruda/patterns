﻿using System.Collections.Generic;

namespace Factory_Hospital
{
    public class AntiviralFactory : MedicinesFactory
    {
        public override List<Medicine> GetLight()
        {
            return new List<Medicine>()
            {
                new Medicine("AntiCovid-Light"),
                new Medicine("CovidStop"),
            };
        }

        public override List<Medicine> GetSerious()
        {
            return new List<Medicine>()
            {
                new Medicine("NoCovid"),
            };
        }
    }
}