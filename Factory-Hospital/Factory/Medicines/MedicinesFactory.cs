﻿using System.Collections.Generic;

namespace Factory_Hospital
{
    public abstract class MedicinesFactory
    {
        public abstract List<Medicine> GetSerious();
        public abstract List<Medicine> GetLight();
    }
}