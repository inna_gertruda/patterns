﻿using System;

namespace Factory_Hospital
{
    public static class PatientsCounter
    {
        private static int Number { get; set; }

        internal static int GetNumber()
        {
            return ++Number;
        }
    }
}