﻿using System;

namespace Factory_Hospital
{
    public class Doctor
    {
        public DoctorType DoctorType { get; set; }

        public Doctor(DoctorType doctorType)
        {
            DoctorType = doctorType;
        }

        public static class Factory
        {
            public static Doctor CreateInfectionist() => new Doctor(DoctorType.Infectionist);

            public static Doctor CreateTherapist() => new Doctor(DoctorType.Therapist);

            public static Doctor CreatePediatrician() => new Doctor(DoctorType.Pediatrician);
        }
    }
}