﻿namespace Factory_Hospital
{
    public class Medicine
    {
        public string Title { get; set; }

        public Medicine(string title)
        {
            Title = title;
        }
    }
}