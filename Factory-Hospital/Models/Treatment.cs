﻿using System.Collections.Generic;

namespace Factory_Hospital
{
    public class Treatment
    {
        public List<Medicine> Medicines { get; set; }

        public Treatment(IEnumerable<Medicine> medicine)
        {
            Medicines = new List<Medicine>();
            Medicines.AddRange(medicine);
        }
    }
}