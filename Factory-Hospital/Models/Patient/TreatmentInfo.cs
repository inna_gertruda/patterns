﻿using System.Collections.Generic;

namespace Factory_Hospital
{
    public class TreatmentInfo
    {
        public List<Treatment> Treatments { get; set; }
        public TreatmentInfo()
        {
            Treatments = new List<Treatment>();
        }
    }
}