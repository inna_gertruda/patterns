﻿using System;
using System.Text;

namespace Factory_Hospital.Models.Patient
{
    public class Patient
    {
        public GeneraInfo GeneraInfo { get; set; }
        public TreatmentInfo TreatmentInfo { get; set; }

        public Patient()
        {

        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.Append($"{GeneraInfo.PatientType} {GeneraInfo.Name} was created with seqNum {GeneraInfo.SeqNumber} on {GeneraInfo.RegistryDate}");

            return sb.ToString();
        }
    }
}
