﻿using System;

namespace Factory_Hospital.Models.Patient
{
    public class GeneraInfo
    {
        public int SeqNumber { get; set; }
        public string Name { get; set; }
        public DateTime RegistryDate { get; set; }
        public PatientType PatientType { get; set; }
        public Doctor Doctor { get; set; }

        internal void SetAutoData()
        {
            SeqNumber = PatientsCounter.GetNumber();
            RegistryDate = DateTime.Now;
        }
    }
}