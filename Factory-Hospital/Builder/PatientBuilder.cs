﻿using Factory_Hospital.Models.Patient;

namespace Factory_Hospital.Builder
{
    public class PatientBuilder
    {
        protected Patient Patient;

        public PatientBuilder()
        {
            Patient = new Patient();
        }

        public PatientBuilder(Patient patient)
        {
            Patient = patient;
        }

        public GeneralInfoBuilder General => new GeneralInfoBuilder(Patient);
        public TreatmentInfoBuilder HasTreatment => new TreatmentInfoBuilder(Patient);

        public static implicit operator Patient(PatientBuilder patientBuilder)
        {
            patientBuilder.Patient.GeneraInfo.SetAutoData();
            return patientBuilder.Patient;
        }
    }
}
