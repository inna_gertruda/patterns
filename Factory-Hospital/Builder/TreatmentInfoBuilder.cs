﻿using Factory_Hospital.Models.Patient;
using System;
using System.Collections.Generic;

namespace Factory_Hospital.Builder
{
    public class TreatmentInfoBuilder : PatientBuilder
    {
        public TreatmentInfoBuilder(Patient patient) : base(patient)
        {
            patient.TreatmentInfo = new TreatmentInfo();
        }

        public TreatmentInfoBuilder Medicines(IEnumerable<Medicine> medicine)
        {
            this.Patient.TreatmentInfo.Treatments.Add(new Treatment(medicine));
            return this;
        }
    }
}
