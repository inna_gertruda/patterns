﻿using Factory_Hospital.Models.Patient;

namespace Factory_Hospital.Builder
{
    public class GeneralInfoBuilder : PatientBuilder
    {
        public GeneralInfoBuilder(Patient patient) : base(patient)
        {
            patient.GeneraInfo = new GeneraInfo();
        }

        public GeneralInfoBuilder Name(string name)
        {
            Patient.GeneraInfo.Name = name;
            return this;
        }

        public GeneralInfoBuilder PatientType(PatientType patientType)
        {
            Patient.GeneraInfo.PatientType = patientType;
            return this;
        }
        public GeneralInfoBuilder Doctor(Doctor doctor)
        {
            Patient.GeneraInfo.Doctor = doctor;
            return this;
        }
    }
}
