﻿namespace Factory_Hospital
{
    public enum DoctorType
    {
        Therapist,
        Pediatrician,
        Infectionist
    }
}